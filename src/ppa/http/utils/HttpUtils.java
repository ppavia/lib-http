package ppa.http.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ppa.httpstream.model.ConcreteHttpStreamBuilder;
import ppa.httpstream.model.HttpStream;
import ppa.httpstream.model.HttpStreamBuildDirector;
import ppa.httpstream.model.HttpStreamBuilder;

public final class HttpUtils {
	private static Logger<HttpUtils> LOG			= Logger.getLogger(HttpUtils.class);
	public static final String SEC_WEBSOCKET_VERSION	= "13";
	public static final String CRLF					= "\r\n";
	public static List<Socket> sockets					= new ArrayList<Socket>();
	
	public static String getHandshakeRequest (
			String requestUri,
			String websocketKey, 
			String origin, 
			String websocketProtocol, 
			String host,
			String websocketExtension) {
		StringBuilder handshake	= new StringBuilder("");
		handshake.append("GET ").append(requestUri).append(" HTTP/1.1").append(CRLF);
		handshake.append("Host: ").append(host).append(CRLF);
		handshake.append("Upgrade: websocket").append(CRLF);
		handshake.append("Connection: Upgrade").append(CRLF);
		handshake.append("Sec-WebSocket-Key: ").append(websocketKey).append(CRLF);
		handshake.append("Origin: ").append(origin).append(CRLF);
		if ( websocketProtocol == null || !websocketProtocol.isEmpty() ) {
			handshake.append("Sec-WebSocket-Protocol: ").append(websocketProtocol).append(CRLF);
		}
		if ( websocketExtension == null || !websocketExtension.isEmpty() ) {
			handshake.append("Sec-WebSocket-Extensions: ").append(websocketExtension).append(CRLF);
		}
		handshake.append("Sec-WebSocket-Version: ").append(SEC_WEBSOCKET_VERSION).append(CRLF);
		handshake.append(CRLF);
		return handshake.toString();
	}
	
	public static String getSockJsRequest (
			String requestUri,
			String accept, 
			String acceptEncoding,
			String acceptLanguage,
			String connection, 
			String dnt,
			String host,
			String referer,
			String userAgent) {
		StringBuilder socketRequest	= new StringBuilder("");
		socketRequest.append("GET ").append(requestUri).append(" HTTP/1.1").append(CRLF);
		socketRequest.append("Accept: ").append(accept).append(CRLF);
		socketRequest.append("Accept-encoding: ").append(acceptEncoding).append(CRLF);
		socketRequest.append("Accept-Language: ").append(acceptLanguage).append(CRLF);
		socketRequest.append("Connection: ").append(connection).append(CRLF);
		socketRequest.append("DNT: ").append(dnt).append(CRLF); // Do Not Track (1 for No Tracking !!)
		socketRequest.append("Host: ").append(host).append(CRLF);
		socketRequest.append("Referer: ").append(referer).append(CRLF);
		if ( userAgent == null || !userAgent.isEmpty() ) {
			socketRequest.append("User-Agent: ").append(userAgent).append(CRLF);
		}
		
		socketRequest.append(CRLF);
		return socketRequest.toString();
	}
	
	public static String processRequest (
			String method,
			String uri,
			String protocol,
			String version, 
			Map<String,String> headerParams) {
		
		StringBuilder socketRequest	= new StringBuilder("");
		socketRequest.append(method).append((char)32)
		.append(uri).append((char)32)
		.append(protocol).append("/").append(version).append(CRLF);
		headerParams.forEach((paramName,paramValue) -> {
			socketRequest.append(paramName).append(": ").append(paramValue).append(CRLF);
		});		
		socketRequest.append(CRLF);
		return socketRequest.toString();
	}
	
	public static void sendRequest (PrintWriter out, String request) {
		out.print(request);
		out.flush();
	}
	
	public static HttpStream readSSLStream (BufferedReader br) {
		HttpStreamBuilder builder			= new ConcreteHttpStreamBuilder();
		HttpStreamBuildDirector director	= new HttpStreamBuildDirector(builder);
		HttpStream response					= director.constructSSL(br);
		return response;
	}
	
	public static HttpStream readSSLStream (InputStream is) {
		HttpStreamBuilder builder			= new ConcreteHttpStreamBuilder();
		HttpStreamBuildDirector director	= new HttpStreamBuildDirector(builder);
		HttpStream response					= director.construct(is);
		return response;
	}
	
	public static HttpStream readStream (BufferedReader br) {
		HttpStreamBuilder builder			= new ConcreteHttpStreamBuilder();
		HttpStreamBuildDirector director	= new HttpStreamBuildDirector(builder);
		HttpStream response					= director.construct(br);
		return response;
	}
	
	public static HttpStream readStream (InputStream is) {
		HttpStreamBuilder builder			= new ConcreteHttpStreamBuilder();
		HttpStreamBuildDirector director	= new HttpStreamBuildDirector(builder);
		HttpStream response					= director.construct(is);
		return response;
	}
	
	public static Map<String,String> mockParamsRequestSockJS () {
		Map<String,String> headerParams	= new HashMap<String,String>();
		headerParams.put("Accept", "*/*");
		headerParams.put("Accept-encoding", "gzip, deflate");
		headerParams.put("Accept-Language", "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3");
		headerParams.put("Connection", "keep-alive");
		headerParams.put("DNT", "1");
		headerParams.put("Host", "localhost:8080");
		headerParams.put("Referer", "http://localhost:8080/");
		return headerParams;
	}
	public static Map<String,String> mockParamsRequestLocalhost () {
		Map<String,String> headerParams	= new HashMap<String,String>();
		headerParams.put("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		headerParams.put("Accept-Language", "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3");
		headerParams.put("Cash-Control", "no-cache");
		headerParams.put("Connection", "keep-alive");
		headerParams.put("Pragma", "no-cache");
		headerParams.put("DNT", "1");
		headerParams.put("Host", "localhost:8081");
		headerParams.put("Upgrade-Insecure-Requests", "1");
		//headerParams.put("If-Modified-Since", "Mon, 27 Aou 2018 06:42:29 GMT");
		headerParams.put("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:61.0) Gecko/20100101 Firefox/61.0");
		
		return headerParams;
	}
	
	public static void scanServerPorts (String host, int nbPortIn, int nbPortOut) {
		Socket socket	= null;
		for ( int i = nbPortIn; i <= nbPortOut; i++ ) {
			try {
				socket = new Socket();
				SocketAddress socAdr = new InetSocketAddress(host, i);
				LOG.info("socAdr = " + socAdr.toString());
				socket.connect(socAdr);
				LOG.info("La machine autorise les connexions sur le port : " + i);
			}
			catch (UnknownHostException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				//System.out.println("server does not autorize port : " + i);
			}
		}
		sockets.add(socket);
	}
	
	public static void closeSockets (List<Socket> sockets) {
		sockets.forEach((socket) -> {
			try {
				socket.close();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
	
}
