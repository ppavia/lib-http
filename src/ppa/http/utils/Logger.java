package ppa.http.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger<T> {
	private Class<T> clazz;
	public static final String EGAL			= " = ";
	public static final String SEP			= " ";
	public static final String INIT			= "<init>";
	public static final String CLINIT		= "<clinit>";
	public static final String CONSTRUCTOR	= "::constructor";
	private static SimpleDateFormat sdfFull	= new SimpleDateFormat("yyyyMMdd HH:mm:ss:SSS");
	private static SimpleDateFormat sdf		= new SimpleDateFormat("HH:mm:ss:SSS");
	
	public static <T> Logger<T> getLogger(Class<T> clazz) {
		return new Logger<T>(clazz);
	}
	
	private Logger(Class<T> clazz) {
		this.setClazz(clazz);
	}
	
	public String logCurrentTime() {
		return "[" + sdf.format(new Date(System.currentTimeMillis())) + "]";
	}

	public String logClass (Class<T> clazz) {
		return "[" + clazz.getSimpleName() + "]";
	}
	
	public String logMethodeName() {
		return logMethodeName(-1);
	}
	
	public String logMethodeName(int level) {
		Throwable t = new Throwable();
		t.fillInStackTrace();
		return logMethodeName(level, t.getStackTrace());
	}
	
	public String logMethodeName(int level, StackTraceElement[] se) {
		String functionName = "";
		if ( level == -1 ) {
			for ( int i=se.length-1; i >= 0; i-- ) {
				if ( se[i].getClassName().equals(this.clazz.getName()) ) {
//					System.out.println("methode : " + se[i].getMethodName());
//					System.out.println("class name : " + se[i].getClassName());
//					System.out.println("native method : " + se[i].isNativeMethod());
//					System.out.println("line : " + se[i].getLineNumber());
					if ( se[i].getMethodName().equals(INIT) ) {
						functionName = this.clazz.getSimpleName() + CONSTRUCTOR;
					}
					else if ( se[i].getMethodName().equals(CLINIT) ) {
						functionName = this.clazz.getSimpleName() + CONSTRUCTOR;
					}
					else {
						functionName	= se[i].getMethodName();
					}
					break;
				}
			}
		}
		else {
			functionName	= se[level].getMethodName();
		}
		return "[" + functionName + "]";
	}
	
	public String logLevel () {
		return logMethodeName(3).toUpperCase();
	}

	public String logHeader (Class<T> clazz) {
		return logCurrentTime() + SEP + logLevel() + SEP + logClass(clazz) + SEP + logMethodeName();
	}

	public Class<T> getClazz() {
		return clazz;
	}

	public void setClazz(Class<T> clazz) {
		this.clazz = clazz;
	}
	
	public void debug (String message) {
		System.out.println(logHeader(this.clazz) + SEP + message);
	}
	public void debug (String nameVar, Object var) {
		System.out.println(logHeader(this.clazz) + SEP + "[" + nameVar + EGAL + var.toString() + "]");
	}
	public void debug (String nameVar, Object var, String message) {
		String mes	= ( message != null ) ? SEP + message : "";
		System.out.println(logHeader(this.clazz) + SEP + "[" + nameVar + EGAL + var.toString() + "]" + mes);
	}
	public void info (String message) {
		System.out.println(logHeader(this.clazz) + SEP + message);
	}
	public void info (String nameVar, Object var) {
		System.out.println(logHeader(this.clazz) + SEP + "[" + nameVar + EGAL + var.toString() + "]");
	}
	public void info (String nameVar, Object var, String message) {
		String mes	= ( message != null ) ? SEP + message : "";
		System.out.println(logHeader(this.clazz) + SEP + "[" + nameVar + EGAL + var.toString() + "]" + mes);
	}
	public void error (String message) {
		System.out.println(logHeader(this.clazz) + SEP + message);
	}
	public void error (String nameVar, Object var) {
		System.out.println(logHeader(this.clazz) + SEP + "[" + nameVar + EGAL + var.toString() + "]");
	}
	public void error (String nameVar, Object var, String message) {
		String mes	= ( message != null ) ? SEP + message : "";
		System.err.println(logHeader(this.clazz) + SEP + "[" + nameVar + EGAL + var.toString() + "]" + mes);
	}
}
