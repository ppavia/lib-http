package ppa.httpstream.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class HttpStream {
	private String code;
	private Map<String, String> headerParams	= new HashMap<String,String>();
	private String protocol;
	private String version;
	private String content;
	private String dataFirstLine;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Map<String, String> getHeaderParams() {
		return headerParams;
	}
	public void setHeaderParams(Map<String, String> headerParams) {
		this.headerParams = headerParams;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getDataFirstLine() {
		return dataFirstLine;
	}
	public void setDataFirstLine(String dataFirstLine) {
		this.dataFirstLine = dataFirstLine;
	}
	public String toString () {
		StringBuilder out	= new StringBuilder("\r\n");
		out.append("HttpStream").append("\r\n");
		out.append("\t---code").append(" : ").append(code).append("\r\n");
		out.append("\t---content").append(" : ").append(content).append("\r\n");
		out.append("\t---protocol").append(" : ").append(protocol).append("\r\n");
		out.append("\t---version").append(" : ").append(version).append("\r\n");
		out.append("\t---dataFirstLine").append(" : ").append(dataFirstLine).append("\r\n");
		out.append("\t---header params").append("\r\n");
		for ( Entry<String,String> headerEntry : headerParams.entrySet() ) {
			out.append("\t---").append(headerEntry.getKey()).append(" : ").append(headerEntry.getValue()).append("\r\n");
		}
		return out.toString();
	}
}
