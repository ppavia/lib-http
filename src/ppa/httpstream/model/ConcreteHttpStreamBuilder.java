package ppa.httpstream.model;

import ppa.httpstream.model.HttpStream;
import ppa.httpstream.model.HttpStreamBuilder;

public class ConcreteHttpStreamBuilder implements HttpStreamBuilder {
	
	private HttpStream httpStream;
	private final static String LINE_HEADER_SEPARATOR	= ":";
	private final static String FIRST_LINE_SEPARATOR	= " ";
	private final static String PROTOCOL_NAME			= "HTTP";
	private final static String PROTOCOL_SEPARATOR		= "/";
	private final static String CONTENT_LENGTH_HEADER	= "Content-Length";
	private int contentLength;
	
	public ConcreteHttpStreamBuilder() {
		this.httpStream	= new HttpStream();
	}

	@Override
	public HttpStreamBuilder buildHeaderParams(String line) {
		getHeaderParameterValue(line);
		return this;
	}

	@Override
	public HttpStreamBuilder buildContent(String line) {
		this.httpStream.setContent(line);
		return this;
	}

	@Override
	public HttpStream build() {
		return this.httpStream;
	}
	
	/**
	 * Insert into a Map<String,String> the header and its value from a response line
	 * @param line
	 */
	public void getHeaderParameterValue (String line) {
		if ( line != null ) {
			String [] lineSplitted	= line.split(LINE_HEADER_SEPARATOR);
			if ( lineSplitted.length > 1 ) {
				String value	= "";
				for ( int i=1; i < lineSplitted.length; i++ ) {
					value += lineSplitted[i];
				}
				this.httpStream.getHeaderParams().put(lineSplitted[0].trim(), value.trim());
				if ( lineSplitted[0].toUpperCase().equals(CONTENT_LENGTH_HEADER.toUpperCase()) ) {
					this.contentLength = Integer.parseInt(value.trim());
				}
			}
			else {
				this.httpStream.getHeaderParams().put(lineSplitted[0].trim(), null);
			}			
		}
	}
	
	@Override
	public HttpStreamBuilder buildStartLineStream (String line) {
		String [] lineSplitted	= line.split(FIRST_LINE_SEPARATOR);
		
		for ( int i=0; i < lineSplitted.length; i++ ) {
			if ( lineSplitted[i].contains(PROTOCOL_NAME) ) {
				String[] infosProtocol	= lineSplitted[i].trim().split(PROTOCOL_SEPARATOR);
				if ( infosProtocol.length > 1 ) {
					this.httpStream.setProtocol(infosProtocol[0]);
					this.httpStream.setVersion(infosProtocol[1]);
				}
			}
			else if ( lineSplitted[i] .length() == 3 && lineSplitted[i].matches("^\\p{Digit}+$") ) {
				this.httpStream.setCode(lineSplitted[i].trim());
			}
			else {
				this.httpStream.setDataFirstLine(lineSplitted[i].trim());
			}
		}
		return this;
	}

	public int getContentLength() {
		return contentLength;
	}

	public void setContentLength(int contentLength) {
		this.contentLength = contentLength;
	}
}
